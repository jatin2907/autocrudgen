"use strict";
$.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
var KTDataTable = function () {
  var datatable;
  var table;

  var initDatatableList = function (tableId, fieldColoumns, ajax_URL, orderColumn, bSearching, data) {
    // Set date data order
    var bSearching = true;
    if (fieldColoumns === undefined) {
      fieldColoumns = [];
    }

    var intial_url = 'http://';
    var intial_url2 = 'https://';
    var final_ajax_url = '';
    if (ajax_URL.indexOf(intial_url) != -1) {
      final_ajax_url = ajax_URL;
    } else if (ajax_URL.indexOf(intial_url2) != -1) {
      final_ajax_url = ajax_URL;
    } else {
      final_ajax_url = base_url + ajax_URL;
    }
    var doms = 'trilp';
    datatable = $(table).DataTable({
      stateSave:true,
      // responsive: true,
      'paging': true,
      "processing": true,
      "info": false,
      'order': orderColumn,
      "serverSide": true,
      "searching": bSearching,
      "orderCellsTop": true,
      "lengthMenu": [10, 25, 50, 75, 100 ],
      'pageLength': 10,
      'ordering': true,
      "columns": fieldColoumns,
      "bPaginate": true,
      "orderCellsTop": true,
      dom: doms,
      initComplete: function () { },
      "ajax": {
        url: final_ajax_url,
        type: "POST",
        global: false,
        "data": function (d) {
          $.extend(d, data);
          d.status = $('.statusFilter').val();
        },
        "error": function () {
        }
      },
      rowCallback: function (row, data) {
        // add a class
        $(row).addClass('text-dark');
      }
    });

    // Re-init functions on every table re-draw -- more info: https://datatables.net/reference/event/draw
    datatable.on('draw', function () {
      // table.search('').columns().search('').draw();
      initToggleToolbar();
      handleDeleteRows();
      toggleToolbars();
      initChangeStatus();
      KTMenu.init(); // reinit KTMenu instances 
    });
    KTDataTable.table = datatable;
    datatable.search('').columns().search('').draw();
    if (bSearching) {
      tableSearchInput(tableId);
    }
  }
  // Init toggle toolbar
  var initToggleToolbar = () => {
    const checkboxes = table.querySelectorAll('tbody .dataIds');
    // Select elements
    const deleteSelected = document.querySelector('[data-kt-table-select="delete_selected"]');

    // Toggle delete selected toolbar
    checkboxes.forEach(c => {
      // Checkbox on click event
      c.addEventListener('click', function () {
        setTimeout(function () {
          toggleToolbars();
        }, 50);
      });
    });

    // Deleted selected rows
    deleteSelected.addEventListener('click', function () {
      Swal.fire({
        text: messages.delete_confirm,
        icon: "warning",
        showCancelButton: true,
        buttonsStyling: false,
        confirmButtonText: "Yes",
        cancelButtonText: "No",
        customClass: {
          confirmButton: "btn fw-bold btn-danger",
          cancelButton: "btn fw-bold btn-active-light-primary"
        }
      }).then(function (result) {
        if (result.value) {
          const allCheckboxes = table.querySelectorAll('tbody [type="checkbox"]');
          let ids = [];
          allCheckboxes.forEach(c => {
            if (c.checked) {
              ids.push(c.value);
            }
          });
          $.ajax({
            url: multiDeleteUrl,
            method: "POST",
            data: {
              id: ids
            },
            success: function (response) {
              swalAlert(response, true, table);
            },
            error: function (response) {
              swalAlert(response, true, table);
            }
          })
        }
      });
    });
  }

  // Toggle toolbars
  const toggleToolbars = () => {
    // Define variables
    const toolbarBase = document.querySelector('[data-kt-table-toolbar="base"]');
    const toolbarSelected = document.querySelector('[data-kt-table-toolbar="selected"]');
    const selectedCount = document.querySelector('[data-kt-table-select="selected_count"]');

    // Select refreshed checkbox DOM elements 
    const allCheckboxes = table.querySelectorAll('tbody .dataIds');

    // Detect checkboxes state & count
    let checkedState = false;
    let count = 0;

    // Count checked boxes
    allCheckboxes.forEach(c => {
      if (c.checked) {
        checkedState = true;
        count++;
      }
    });

    // Toggle toolbars
    if (checkedState) {
      selectedCount.innerHTML = count;
      if (toolbarBase != null)
        toolbarBase.classList.add('d-none');
      toolbarSelected.classList.remove('d-none');
    } else {
      if (toolbarBase != null)
        toolbarBase.classList.remove('d-none');
      toolbarSelected.classList.add('d-none');
    }
  }
  // Delete customer
  var handleDeleteRows = () => {
    // Select all delete buttons
    const deleteButtons = table.querySelectorAll('tbody [data-kt-table-filter="delete_row"]');

    deleteButtons.forEach(d => {
      // Delete button on click
      d.addEventListener('click', function (e) {
        e.preventDefault();
        var deleteUrl = $(this).attr('data-url');
        var id = $(this).attr('data-id');

        Swal.fire({
          text: messages.delete_confirm,
          icon: "warning",
          showCancelButton: true,
          buttonsStyling: false,
          confirmButtonText: "Yes, delete!",
          cancelButtonText: "No, cancel",
          customClass: {
            confirmButton: "btn fw-bold btn-danger",
            cancelButton: "btn fw-bold btn-active-light-primary"
          }
        }).then(function (result) {
          if (result.value) {
            $.ajax({
              url: deleteUrl,
              type: "DELETE",
              data: {
                id: id
              },
              success: function (response) {
                swalAlert(response, true, table);
              }
            })
          }
        });
      })
    });
  }
  var initChangeStatus = () => {
    const status = table.querySelectorAll('tbody .changeStatus');
    status.forEach(d => {
      d.addEventListener('click', function (e) {
        var updateStatusUrl = $(this).attr('data-url');
        Swal.fire({
          text: messages.status_confirm,
          icon: "warning",
          showCancelButton: true,
          buttonsStyling: false,
          confirmButtonText: "Yes",
          cancelButtonText: "No",
          customClass: {
            confirmButton: "btn fw-bold btn-danger",
            cancelButton: "btn fw-bold btn-active-light-primary"
          }
        }).then(function (result) {
          if (result.value) {
            $.ajax({
              url: updateStatusUrl,
              type: "POST",
              success: function (response) {
                swalAlert(response, true, table);
              }
            })
          }
        })
      })
    })
  }
  var handleSearchDatatable = () => {
    const filterSearch = document.querySelector('#search');
    filterSearch.addEventListener('keyup', function (e) {
      datatable.search(e.target.value).draw();
    });
  }

  var tableSearchInput = (tableId) => {
    var r = $(tableId + ' tfoot tr');
    $(tableId+' select').val('');
    $(tableId + ' thead').append(r);
    var table = KTDataTable.table;
    table.columns().every(function (colindex) {
      var column = this;
      column.search('');
      $('.tbl-filter-column').val('');
      var tColumn = $(tableId + ' thead th').eq(this.index());
      $('input', this.footer()).on('keyup change', function () {
        column.search($.trim($(this).val()), false, false, true).draw();
      });
      $('select', this.footer()).on('change', function () {
        table.draw();
      });
    });
  }

  KTDataTable.prototype.init = function (tableId, fieldColoumns, ajax_URL, orderColumn) {
    tableId = tableId;
    table = document.querySelector(tableId)
    if (!table) {
      return;
    }
    initDatatableList(tableId, fieldColoumns, ajax_URL, orderColumn);
    initToggleToolbar();
    toggleToolbars();
    handleSearchDatatable();
  }
}

function swalAlert(response, datatable = false, table) {
  if (response.status == true) {
    if (datatable == true) {
      Swal.fire({
        text: response.message,
        icon: "success",
        buttonsStyling: false,
        confirmButtonText: "Ok, got it!",
        customClass: {
          confirmButton: "btn fw-bold btn-primary",
        }
      }).then(function () {
        $(table).DataTable().ajax.reload(null, false);
        const headerCheckbox = table.querySelectorAll('[type="checkbox"]')[0];
        headerCheckbox.checked = false;
      });
    }
  } else {
    Swal.fire({
      text: response.message,
      icon: "error",
      buttonsStyling: false,
      confirmButtonText: "Ok, got it!",
      customClass: {
        confirmButton: "btn fw-bold btn-primary",
      }
    });
  }
}

$('.selectAll').on('change', function (e) {
  var check = this;
  var targets = document.querySelectorAll(check.getAttribute('data-kt-check-target'));
  KTUtil.each(targets, function (target) {
    if (target.type == 'checkbox' && $(target).hasClass('dataIds') == true) {
      target.checked = check.checked;
    } else {
      target.classList.toggle('active');
    }
    let checkedState = false;
    let tableId = $(check).attr('data-kt-table');
    let table = document.querySelector(`#${tableId}`);
    const allCheckboxes = table.querySelectorAll('tbody .dataIds');;
    let count = 0;
    // Count checked boxes
    allCheckboxes.forEach(c => {
      if (c.checked) {
        checkedState = true;
        count++;
      }
    });
    const toolbarBase = document.querySelector('[data-kt-table-toolbar="base"]');
    const toolbarSelected = document.querySelector('[data-kt-table-toolbar="selected"]');
    const selectedCount = document.querySelector('[data-kt-table-select="selected_count"]');

    // Toggle toolbars
    if (checkedState) {
      selectedCount.innerHTML = count;
      toolbarBase.classList.add('d-none');
      toolbarSelected.classList.remove('d-none');
    } else {
      toolbarBase.classList.remove('d-none');
      toolbarSelected.classList.add('d-none');
    }
  });
})

