
@if (session('success'))
    <div id="alertMessages" class="fv-row mb-8">
        <div class="alert alert-success d-flex align-items-center p-5">
            <i class="fa fa-check text-success me-4"><span class="path1"></span><span class="path2"></span></i>
            <div class="d-flex flex-column">
                <span>{{ @session('success') }}</span>
            </div>
        </div>
    </div>
@endif
@if (session('error'))
    <div id="alertMessages" class="fv-row mb-8">
        <div class="alert alert-danger d-flex align-items-center p-5">
            <i class="fa fa-check text-danger me-4"><span class="path1"></span><span class="path2"></span></i>
            <div class="d-flex flex-column">
                <span>{{ @session('error') }}</span>
            </div>
        </div>
    </div>
@endif
@if ($errors->any() && $errors->has('error'))
    <div id="alertMessages" class="fv-row mb-8">
        <div class="alert alert-danger d-flex align-items-center p-5">
            <i class="fa fa-times text-danger me-4"><span class="path1"></span><span class="path2"></span></i>
            <div class="d-flex flex-column">
                <span>{{ $errors->first('error') }}</span>
            </div>
        </div>
    </div>
@endif
