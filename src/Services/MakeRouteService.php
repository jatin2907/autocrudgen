<?php

namespace Oneclick\Autocrudgen\Services;


use Illuminate\Support\Facades\File;
use Illuminate\Console\Concerns\InteractsWithIO;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Support\Str;


class MakeRouteService
{
    use InteractsWithIO;

    public PathsAndNamespacesService $pathsAndNamespacesService;
    public function __construct(
        PathsAndNamespacesService $pathsAndNamespacesService,
        ConsoleOutput $consoleOutput,
        Application $application
    ) {
        $this->pathsAndNamespacesService = $pathsAndNamespacesService;
        $this->output = $consoleOutput;
        $this->laravel = $application->getNamespace();
    }

    public function replaceContentRouteStub($namingConvention)
    {
        $routeStub = File::get($this->pathsAndNamespacesService->getRouteStubPath());
        $routeStub = str_replace('MIDDLEWARENAME',config('autocrudgen.middleware'), $routeStub);
        $routeStub = str_replace('DummySingularName', $namingConvention['singular_name'], $routeStub);
        $routeStub = str_replace('DummySingularLowName', $namingConvention['singular_low_name'], $routeStub);
        $routeStub = str_replace('DummyPluralName', $namingConvention['plural_name'], $routeStub);
        $routeStub = str_replace('DummyPluralLowName', $namingConvention['plural_low_name'], $routeStub);
        return $routeStub;
    }

    public function makeCompleteRouteFile($namingConvention, $columns)
    {
        $routeStub = $this->replaceContentRouteStub($namingConvention);
        $routeStub = $this->findAndReplaceRoute(config('autocrudgen.route_file'), $routeStub,$namingConvention);
    }

    public function findAndReplaceRoute($path,$routeStub,$namingConvention){
        $routePath = $this->pathsAndNamespacesService->getRealpathBaseRoute($path);
        if (File::exists($routePath)) {
            File::append($routePath, $routeStub);
            $this->line("<info>" . $namingConvention['plural_name'].' routes added </info>');
        } else{
            $this->error('Invalid Route path');
        }
    }
}