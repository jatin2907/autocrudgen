<?php

namespace Oneclick\Autocrudgen\Services;

class DataType {
    public function getDataType(){
        $datatypes = [
            'bigIncrements'=>[],
            'bigInteger'=>[],
            'binary'=>[],
            'boolean' ,
            'char' => [
                'length' => 200,
            ],
            'dateTimeTz' => [
                'precision' => 0,
            ],
            'dateTime'=>[],
            'date'=>[],
            'decimal' => [
                'precision' => 8,
                'scale' => 2,
            ],
            'double' => [
                'precision' => 8,
                'scale' => 2,
            ],
            'enum' => [
                'array' => []
            ],
            'float' => [
                'precision' => 8,
                'scale' => 2,
            ],
            'foreignId'=>[
                'constrained' => '',
            ],
            'foreignIdFor' => [
                'class' => '',
            ],
            'foreignUlid'=>[
                'constrained' => '',
            ],
            'foreignUuid'=>[
                'constrained' => '',
            ],
            'geometryCollection'=>[],
            'geometry'=>[],
            'id'=>[],
            'increments'=>[],
            'integer'=>[],
            'ipAddress'=>[],
            'json'=>[],
            'jsonb'=>[],
            'lineString'=>[],
            'longText'=>[],
            'macAddress'=>[],
            'mediumIncrements'=>[],
            'mediumInteger'=>[],
            'mediumText'=>[],
            'morphs'=>[],
            'multiLineString'=>[],
            'multiPoint' ,
            'multiPolygon' ,
            'nullableMorphs'=>[],
            'nullableTimestamps'=>[],
            'nullableUlidMorphs'=>[],
            'nullableUuidMorphs'=>[],
            'point'=>[],
            'polygon'=>[],
            'rememberToken'=>[],
            'set' => [
                'array' => []
            ],
            'smallIncrements'=>[],
            'smallInteger'=>[],
            'softDeletesTz' => [
                'precision' => 0
            ],
            'softDeletes' => [
                'precision' => 0
            ],
            'string' => [
                'length' => 100,
            ],
            'text'=>[],
            'timeTz'  => [
                'precision' => 0
            ],
            'time'  => [
                'precision' => 0
            ],
            'timestampTz'  => [
                'precision' => 0
            ],
            'timestamp'  => [
                'precision' => 0
            ],
            'timestampsTz' => [
                'precision' => 0
            ],
            'timestamps' => [
                'precision' => 0
            ],
            'tinyIncrements' => [
                'precision' => 0
            ],
            'tinyInteger'  => [
                'precision' => 0
            ],
            'tinyText' => [
                'precision' => 0
            ],
            'unsignedBigInteger'=>[],
            'unsignedDecimal' => [
                'precision' => 8,
                'scale' => 2,
            ],
            'unsignedInteger'=>[],
            'unsignedMediumInteger'=>[],
            'unsignedSmallInteger'=>[],
            'unsignedTinyInteger'=>[],
            'ulidMorphs'=>[],
            'uuidMorphs'=>[],
            'ulid'=>[],
            'uuid'=>[],
            'year'=>[],
        ];
        return $datatypes;
    }
}
