<?php

namespace Oneclick\Autocrudgen\Services;

use Exception;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\File;
use Illuminate\Console\Concerns\InteractsWithIO;
use Oneclick\Autocrudgen\Services;
use Symfony\Component\Console\Output\ConsoleOutput;

class MakeMigrationService
{
    use InteractsWithIO;

    public PathsAndNamespacesService $pathsAndNamespacesService;
    public MakeGlobalService $makeGlobalService;
    public function __construct(
        PathsAndNamespacesService $pathsAndNamespacesService,
        ConsoleOutput $consoleOutput,
        MakeGlobalService $makeGlobalService
    ) {
        $this->pathsAndNamespacesService = $pathsAndNamespacesService;
        $this->output = $consoleOutput;
        $this->makeGlobalService = $makeGlobalService;
    }

    public function replaceContentMigrationStub($namingConvention)
    {
        $migrationStub = File::get($this->pathsAndNamespacesService->getMigrationStubPath());
        $table         = $namingConvention['table_name'];
        $migrationStub = str_replace('DummyTable', $table, $migrationStub);
        $migrationStub = str_replace('DummyClass', Str::studly('create_' . $table . '_table'), $migrationStub);

        return $migrationStub;
    }

    public function findAndReplaceMigrationPlaceholderColumns($columns, $migrationStub)
    {
        $fieldsMigration = '';
        $dataTyeObj = new DataType();
        $dataTypes = $dataTyeObj->getDataType();
        // we create our placeholders regarding columns

        // check if id column exist
        $string = implode(',', $columns);
        $isContainIdColumn = str_contains($string, 'id:');
        if ($isContainIdColumn == false) {
            $fieldsMigration .= str_repeat("\t", 3) . '$table->id();'."\n";
        }
        foreach ($columns as $column) {
            $type     = explode(':', trim($column));
            $columnName = $type[0];

            $dataObj =  $type[1];
            $columnDetail = explode(' ', trim($dataObj));
            $dataTypeName = $columnDetail[0];

            if (array_key_exists('length', $dataTypes[$dataTypeName])) {
                if (!isset($columnDetail[1])) {
                    $lengthDetail = [$dataTypes[$dataTypeName]['length']];
                } else {
                    $lengthDetail = json_decode($columnDetail[1]);
                }
                $length = $lengthDetail[0];
                $fieldsMigration .= str_repeat("\t", 3) . '$table' . "->$dataTypeName('" . trim($columnName) . "'," . $length . ");\n";
            } else if (array_key_exists('array', $dataTypes[$dataTypeName])) {
                if (!isset($columnDetail[1])) {
                    throw new Exception('invalid column ' . $dataTypes[$dataTypeName] . ' parameter');
                }
                $values = str_replace('[', '', str_replace(']', '', explode('|', $columnDetail[1])));
                $values = implode(',', $values);
                $fieldsMigration .= str_repeat("\t", 3) . '$table' . "->$dataTypeName('" . trim($columnName) . "',[" . $values . "]);\n";
            } else if (array_key_exists('precision', $dataTypes[$dataTypeName]) && array_key_exists('scale', $dataTypes[$dataTypeName])) {
                if (!isset($columnDetail[1])) {
                    $values = $dataTypes[$dataTypeName]['precision'] . ',' . $dataTypes[$dataTypeName]['scale'];
                } else {
                    $values = str_replace('[', '', str_replace(']', '', explode('|', $columnDetail[1])));
                    $values = implode(',', $values);
                }
                $fieldsMigration .= str_repeat("\t", 3) . '$table' . "->$dataTypeName('" . trim($columnName) . "'," . $values . ");\n";
            } else if (array_key_exists('precision', $dataTypes[$dataTypeName])) {
                if (!isset($columnDetail[1])) {
                    $values = $dataTypes[$dataTypeName]['precision'];
                } else {
                    $values = str_replace('[', '', str_replace(']', '', explode('|', $columnDetail[1])));
                    $values = implode(',', $values);
                }
                $fieldsMigration .= str_repeat("\t", 3) . '$table' . "->$dataTypeName('" . trim($columnName) . "'," . $values . ");\n";
            } else if (array_key_exists('constrained', $dataTypes[$dataTypeName])) {
                if (!isset($columnDetail[1])) {
                    $constrained = "->constrained()->onDelete('cascade')";
                } else {
                    $values = str_replace('[', '', str_replace(']', '', explode('|', $columnDetail[1])));
                    $refrenceId = '';
                    if (count($values) > 1) {
                        $refrenceId = ',' . $values[1];
                    }
                    $constrained = "->constrained(" . $values[0] . "" . $refrenceId . ")->onDelete('cascade')";
                }
                $fieldsMigration .= str_repeat("\t", 3) . '$table' . "->$dataTypeName('" . trim($columnName) . "')" . $constrained . ";\n";
            } else if (array_key_exists('class', $dataTypes[$dataTypeName])) {
                if (!isset($columnDetail[1])) {
                    $constrained = "->constrained()->onDelete('cascade')";
                } else {
                    $values = str_replace('[', '', str_replace(']', '', explode('|', $columnDetail[1])));
                    $refrenceId = '';
                    if (count($values) > 1) {
                        $refrenceId = ',' . $values[1];
                    }
                    $constrained = "->constrained(" . $values[0] . "" . $refrenceId . ")->onDelete('cascade')";
                }
                $columnName = 'App\Models\\' . $columnName . '::class';
                $fieldsMigration .= str_repeat("\t", 3) . '$table' . "->$dataTypeName(" . trim($columnName) . ")" . $constrained . ";\n";
            } else {
                $fieldsMigration .= str_repeat("\t", 3) . '$table' . "->$dataTypeName('" . trim($columnName) . "');\n";
            }
        }
        $fieldsMigration = $this->makeGlobalService->cleanLastLineBreak($fieldsMigration);
        // we replace our placeholders
        $migrationStub = str_replace('DummyFields', $fieldsMigration, $migrationStub);
        return $migrationStub;
    }

    public function generateNameMigrationFile($date, $namingConvention)
    {
        return $date . '_create_' . $namingConvention['table_name'] . '_table.php';
    }

    public function createMigrationFile($migrationStub, $namingConvention)
    {
        $date = date('Y_m_d_His');
        File::put($this->pathsAndNamespacesService->getRealpathBaseMigration() . $this->generateNameMigrationFile($date, $namingConvention), $migrationStub);
        $this->line("<info>Created Migration:</info> $date" . "_create_" . $namingConvention['table_name'] . "_table.php");
    }

    public function makeCompleteMigrationFile($namingConvention, $columns)
    {
        $migrationStub = $this->replaceContentMigrationStub($namingConvention);
        $migrationStub = $this->findAndReplaceMigrationPlaceholderColumns($columns, $migrationStub);
        $this->createMigrationFile($migrationStub, $namingConvention);
    }
}
