<?php

namespace Oneclick\Autocrudgen\Services;


use Illuminate\Support\Facades\File;
use Illuminate\Console\Concerns\InteractsWithIO;
use Symfony\Component\Console\Output\ConsoleOutput;
use Illuminate\Contracts\Foundation\Application;

class MakeViewsService
{
    use InteractsWithIO;

    public PathsAndNamespacesService $pathsAndNamespacesService;
    public function __construct(
        PathsAndNamespacesService $pathsAndNamespacesService,
        ConsoleOutput $consoleOutput,
        Application $application
    ) {
        $this->pathsAndNamespacesService = $pathsAndNamespacesService;
        $this->output = $consoleOutput;
        $this->laravel = $application->getNamespace();
    }

    public function createDirectoryViews($namingConvention)
    {
        $directoryName = $this->pathsAndNamespacesService->getRealpathBaseCustomViews($namingConvention);
        // if the directory doesn't exist we create it
        if (!File::isDirectory($directoryName)) {
            File::makeDirectory($directoryName, 0755, true);
            $this->line("<info>Created views directory:</info> " . $namingConvention['plural_low_name']);
        } else
            $this->error('Views directory ' . $namingConvention['plural_low_name'] . ' already exists');
    }

    public function replaceContentControllerStub($namingConvention, $laravelNamespace)
    {
        $controllerStub = File::get($this->pathsAndNamespacesService->getControllerStubPath());
        $controllerStub = str_replace('DummyClass', $namingConvention['plural_name'] . 'Controller', $controllerStub);
        $controllerStub = str_replace('DummyModel', $namingConvention['singular_name'], $controllerStub);
        $controllerStub = str_replace('DummyVariableSing', $namingConvention['singular_low_name'], $controllerStub);
        $controllerStub = str_replace('DummyVariable', $namingConvention['plural_low_name'], $controllerStub);
        $controllerStub = str_replace('DummyNamespace', $this->pathsAndNamespacesService->getDefaultNamespaceController($laravelNamespace), $controllerStub);
        $controllerStub = str_replace('DummyRootNamespace', $laravelNamespace, $controllerStub);
        return $controllerStub;
    }

    public function findAndReplaceControllerPlaceholderColumns($columns, $controllerStub, $namingConvention)
    {
        $cols = '';
        foreach ($columns as $column) {
            $type     = explode(':', trim($column));
            $column   = $type[0];

            // our placeholders
            $cols .= str_repeat("\t", 2) . 'DummyCreateVariableSing$->' . trim($column) . '=$request->input(\'' . trim($column) . '\');' . "\n";
        }

        // we replace our placeholders
        $controllerStub = str_replace('DummyUpdate', $cols, $controllerStub);
        $controllerStub = str_replace('DummyCreateVariable$', '$' . $namingConvention['plural_low_name'], $controllerStub);
        $controllerStub = str_replace('DummyCreateVariableSing$', '$' . $namingConvention['singular_low_name'], $controllerStub);

        return $controllerStub;
    }

    public function findAndReplaceIndexViewPlaceholderColumns($columns, $templateViewsDirectory, $namingConvention, $separateStyleAccordingToActions)
    {
        $thIndex = $indexView = $tfIndex = '';
        $dataTyeObj = new DataType();
        $dataTypes = $dataTyeObj->getDataType();

        $string = implode(',', $columns);
        $isContainIdColumn = str_contains($string, 'id:');
        $idHtml = '<th class="w-10px pe-2">'."\n";
        $idHtml .= str_repeat("\t", 12) .'<div class="form-check form-check-sm form-check-custom form-check-solid me-3">'."\n";
        $idHtml .= str_repeat("\t", 13) .'<input class="form-check-input selectAll" type="checkbox" data-kt-check-target="#DummySingularLowNameTable .form-check-input" value="1" data-kt-table="DummySingularLowNameTable" />'."\n";
        $idHtml .= str_repeat("\t", 12) .'</div>'."\n";
        $idHtml .= str_repeat("\t", 11) .'</th>'."\n";
        if ($isContainIdColumn == false) {
            $thIndex .= $idHtml;
            $tfIndex .= "<td></td>\n";
        }

        foreach ($columns as $column) {
            $type      = explode(':', trim($column));
            $column    = $type[0];
            $columnName = $type[0];
            $dataObj =  $type[1];
            $columnDetail = explode(' ', trim($dataObj));
            $dataTypeName = $columnDetail[0];

            $dataObj =  $type[1];
            $columnDetail = explode(' ', trim($dataObj));
            $dataTypeName = $columnDetail[0];
            $label = ucfirst(str_replace('_',' ',trim($columnName)));

            if (!str_contains($dataTypeName, 'foreign')) {

                if (trim($columnName) == 'id') {
                    $thIndex .= $idHtml;
                    $tfIndex .= str_repeat("\t", 11) .'<td></td>'."\n";
                } else if (array_key_exists('array', $dataTypes[$dataTypeName])) {
                    $optionArray = str_replace('[', '', str_replace(']', '', explode('|', $columnDetail[1])));
                    $optionString = "[ ''=> '" . trim(ucfirst($columnName)) . "',";
                    foreach ($optionArray as $key => $option) {
                        $optionString .= $option . ' =>' . $option . ",";
                    }
                    $optionString .= ']';
                    $column = "{!! Form::select(''," . $optionString . ", null,['class' =>'form-select form-select-solid statusFilter tbl-filter-column','data-control'=>'select2','data-kt-select2' => 'true','data-hide-search' => 'true']) !!}";
                    $thIndex    .= str_repeat("\t", 11) . "<th>" . $label . "</th>\n";
                    $tfIndex    .= str_repeat("\t", 11) . "<td>" . $column . "</td>\n";
                } else {
                    // our placeholders
                    $column = "{!! Form::text('',null,['class'=>'form-control form-control-solid tbl-filter-column','maxlength'=>config('validation.input_length'), 'placeholder'=> '" . $label . "',]) !!}";
                    $thIndex    .= str_repeat("\t", 11) . "<th>" . $label . "</th>\n";
                    $tfIndex .= str_repeat("\t", 11) . "<td>"  .$column ."</td>\n";
                    $indexView  .= str_repeat("\t", 11) . "<td>{{ DummyCreateVariableSing$->" . $column . " }}</td>\n";
                }
            }
        }
        $indexStub = File::get($this->pathsAndNamespacesService->getCrudgenViewsStubCustom($templateViewsDirectory) . DIRECTORY_SEPARATOR . 'index.stub');
        $indexStub = str_replace('DummyExtends', $separateStyleAccordingToActions['index']['extends'], $indexStub);
        $indexStub = str_replace('DummyTitle', $separateStyleAccordingToActions['index']['section_title'], $indexStub);
        $indexStub = str_replace('DummySection', $separateStyleAccordingToActions['index']['section'], $indexStub);
        $indexStub = str_replace('DummyHeaderTable', $thIndex, $indexStub);
        $indexStub = str_replace('DummyFooterTable', $tfIndex, $indexStub);
        $indexStub = str_replace('DummySingularName', $namingConvention['singular_name'], $indexStub);
        $indexStub = str_replace('DummySingularLowName', $namingConvention['singular_low_name'], $indexStub);
        $indexStub = str_replace('DummyPluralName', $namingConvention['plural_name'], $indexStub);
        $indexStub = str_replace('DummyPluralLowName', $namingConvention['plural_low_name'], $indexStub);

        return $indexStub;
    }

    public function findAndReplaceIndexScriptPlaceholderColumns($columns, $templateViewsDirectory, $namingConvention, $separateStyleAccordingToActions)
    {
        $columnsStub = $columnsDetailsStub = '';
        $string = implode(',', $columns);
        $isContainIdColumn = str_contains($string, 'id:');
        $idColumn = "{
            'data': 'id',
            'name':'checkbox',
            render(data, type, row) {
              return row.checkbox;
            },
            orderable: false,
            searchable: false
        },\n";
        if ($isContainIdColumn == false) {
            $columnsStub .= $idColumn;
        }
        foreach ($columns as $key => $column) {
            $type      = explode(':', trim($column));
            $column    = $type[0];
            $columnName = $type[0];
            $dataObj =  $type[1];
            $columnDetail = explode(' ', trim($dataObj));
            $dataTypeName = $columnDetail[0];
            if (!str_contains($dataTypeName, 'foreign')) {
                if (trim($columnName) == 'id') {
                    $columnsStub .= $idColumn;
                } else {
                    $columnsStub .= "{\n data: '" . $column . "'\n},\n";
                }
            }
            // our placeholders
        }
        $columnsCount = 3 + count($columns);
        $indexStub = File::get($this->pathsAndNamespacesService->getCrudgenViewsStubCustom($templateViewsDirectory) . DIRECTORY_SEPARATOR . 'table-datatables.stub');
        $indexStub = str_replace('DummySingularName', $namingConvention['singular_name'], $indexStub);
        $indexStub = str_replace('DummySingularLowName', $namingConvention['singular_low_name'], $indexStub);
        $indexStub = str_replace('DummyPluralName', $namingConvention['plural_name'], $indexStub);
        $indexStub = str_replace('DummyPluralLowName', $namingConvention['plural_low_name'], $indexStub);
        $indexStub = str_replace('DummyColumnsStub', $columnsStub, $indexStub);
        // $indexStub = str_replace('DummyColumnsDetailStub', $columnsDetailsStub, $indexStub);
        // $indexStub = str_replace('DummyColumnsCountStub', $columnsCount, $indexStub);

        return $indexStub;
    }

    public function findAndReplaceCreateViewPlaceholderColumns($columns, $templateViewsDirectory, $namingConvention, $separateStyleAccordingToActions)
    {
        $formCreate = '';
        $dataTyeObj = new DataType();
        $dataTypes = $dataTyeObj->getDataType();
        $formCreate .= "<div class='row row-cols-1 row-cols-md-2'>\n";
        foreach ($columns as $column) {
            $type      = explode(':', trim($column));
            $columnName = $type[0];
            if (!str_contains($type[1], 'foreign') && $columnName != 'id') {
                $dataObj =  $type[1];
                $columnDetail = explode(' ', trim($dataObj));
                $label = ucfirst(str_replace('_',' ',trim($columnName)));
                $dataTypeName = $columnDetail[0];
                $typeHtml = $this->getHtmlType($dataTypeName);
                $formCreate .= str_repeat("\t", 8) . '<div class="col">';
                $formCreate .= str_repeat("\t", 9) ."\n".'<div class="fv-row mb-7 fv-plugins-icon-container">' . "\n";
                $formCreate .= str_repeat("\t", 3) . '{{ Form::label(\'' . trim($columnName) . '\', \'' . $label .'\',[\'class\'=>\'d-flex fs-5 fw-semibold mb-4 required\']) }}' . "\n";
                if (array_key_exists('array', $dataTypes[$dataTypeName])) {
                    $optionArray = str_replace('[', '', str_replace(']', '', explode('|', $columnDetail[1])));
                    $optionString = "[ ''=> '" . trim(ucfirst($columnName)) . "',";
                    foreach ($optionArray as $key => $option) {
                        $optionString .= $option . ' =>' . $option . ",";
                    }
                    $optionString .= ']';
                    $formCreate .= str_repeat("\t", 3) . '{{ Form::' . $typeHtml . '(\'' . trim($columnName) . '\', ' . $optionString . ', null,[\'class\' => \'form-select form-select-solid\', \'data-control\'=>\'select2\',\'data-kt-select2\' => \'true\',\'data-hide-search\' => \'true\']) }}' . "\n";
                } else {
                    // our placeholders
                    $formCreate .= str_repeat("\t", 3) . '{{ Form::' . $typeHtml . '(\'' . trim($columnName) . '\', null, [\'class\' => \'form-control form-control-solid mb-4 required\',\'placeholder\'=>\''.$label.'\']) }}' . "\n";
                }
                $formCreate .= "@error('". trim($columnName) ."')<div class='text-sm text-danger'>{{ \$message }}</div>@enderror";
                $formCreate .= str_repeat("\t", 2) . '</div></div>' . "\n";
            }
        }
        $formCreate .= str_repeat("\t", 2) . '</div>'. "\n";

        $createStub = File::get($this->pathsAndNamespacesService->getCrudgenViewsStubCustom($templateViewsDirectory) . DIRECTORY_SEPARATOR . 'create.stub');

        $createStub = str_replace('DummyExtends', $separateStyleAccordingToActions['create']['extends'], $createStub);
        $createStub = str_replace('DummyTitle', $separateStyleAccordingToActions['create']['section_title'], $createStub);
        $createStub = str_replace('DummySection', $separateStyleAccordingToActions['create']['section'], $createStub);
        $createStub = str_replace('DummyFormCreate', $formCreate, $createStub);
        $createStub = str_replace('DummySingularName', $namingConvention['singular_name'], $createStub);
        $createStub = str_replace('DummyPluralName', $namingConvention['plural_name'], $createStub);
        $createStub = str_replace('DummyPluralLowName', $namingConvention['plural_low_name'], $createStub);

        return $createStub;
    }

    public function findAndReplaceShowViewPlaceholderColumns($templateViewsDirectory, $namingConvention, $separateStyleAccordingToActions, $columns)
    {
        $formDetail = '';
        $formDetail .= '<div class="row row-cols-1 row-cols-md-2">';
        foreach ($columns as $column) {
            $type      = explode(':', trim($column));
            $columnName = $type[0];
            if (!str_contains($type[1], 'foreign') && $columnName != 'id') {
                $dataObj =  $type[1];
                $columnDetail = explode(' ', trim($dataObj));
                $label = ucfirst(str_replace('_',' ',trim($columnName)));
                $dataTypeName = $columnDetail[0];
                $typeHtml = $this->getHtmlType($dataTypeName);
                $formDetail .= str_repeat("\t", 2) . '<div class="col"><div class="fv-row mb-7 fv-plugins-icon-container">' . "\n";
                $formDetail .= str_repeat("\t", 3) . '{{ Form::label(\'' . trim($columnName) . '\', \'' . $label .'\',[\'class\'=>\'fs-4 text-dark fw-bold text-hover-primary text-dark lh-base\']) }}' . "\n";
                $formDetail .= str_repeat("\t", 3) . '<div class="fw-semibold fs-5 text-gray-600 text-dark my-4">{{@$'.$namingConvention['singular_low_name'].'->'.trim($columnName).'}}</div>';
                $formDetail .= str_repeat("\t", 2) . '</div></div>' . "\n";
            }
        }
        $formDetail .= str_repeat("\t", 2) . '</div>'. "\n";

        $showStub = File::get($this->pathsAndNamespacesService->getCrudgenViewsStubCustom($templateViewsDirectory) . DIRECTORY_SEPARATOR . 'show.stub');
        $showStub = str_replace('DummyCreateVariableSing$', '$' . $namingConvention['singular_low_name'], $showStub);
        $showStub = str_replace('DummyExtends', $separateStyleAccordingToActions['show']['extends'], $showStub);
        $showStub = str_replace('DummySection', $separateStyleAccordingToActions['show']['section'], $showStub);
        $showStub = str_replace('DummyFormCreate', $formDetail, $showStub);
        $showStub = str_replace('DummySingularName', $namingConvention['singular_name'], $showStub);
        $showStub = str_replace('DummyPluralName', $namingConvention['plural_name'], $showStub);
        $showStub = str_replace('DummyPluralLowName', $namingConvention['plural_low_name'], $showStub);
        return $showStub;
    }

    public function findAndReplaceEditViewPlaceholderColumns($columns, $templateViewsDirectory, $namingConvention, $separateStyleAccordingToActions)
    {
        $formEdit = '';
        $formEdit .= '<div class="row row-cols-1 row-cols-md-2">';
        foreach ($columns as $column) {
            $dataTyeObj = new DataType();
            $dataTypes = $dataTyeObj->getDataType();
            $type      = explode(':', trim($column));
            $columnName = $type[0];
            if (!str_contains($type[1], 'foreign') && $columnName != 'id') {
                $dataObj =  $type[1];
                $label = ucfirst(str_replace('_',' ',trim($columnName)));
                $columnDetail = explode(' ', trim($dataObj));
                $dataTypeName = $columnDetail[0];
                $typeHtml = $this->getHtmlType($dataTypeName);
                $formEdit .= str_repeat("\t", 2) . '<div class="col"><div class="fv-row mb-7 fv-plugins-icon-container">' . "\n";
                $formEdit .= str_repeat("\t", 3) . '{{ Form::label(\'' . trim($columnName) . '\', \'' . $label .'\',[\'class\'=>\'d-flex fs-5 fw-semibold mb-4 required\']) }}' . "\n";
                if (array_key_exists('array', $dataTypes[$dataTypeName])) {
                    $optionArray = str_replace('[', '', str_replace(']', '', explode('|', $columnDetail[1])));
                    $optionString = "[ ''=> '" . trim(ucfirst($columnName)) . "',";
                    foreach ($optionArray as $key => $option) {
                        $optionString .= $option . ' =>' . $option . ",";
                    }
                    $optionString .= ']';
                    $formEdit .= str_repeat("\t", 3) . '{{ Form::' . $typeHtml . '(\'' . trim($columnName) . '\', ' . $optionString . ', @$'.$namingConvention['singular_low_name'].'->'.trim($columnName).',[\'class\' => \'form-select form-select-solid\', \'data-control\'=>\'select2\',\'data-kt-select2\' => \'true\',\'data-hide-search\' => \'true\']) }}' . "\n";
                } else {
                    // our placeholders
                    $formEdit .= str_repeat("\t", 3) . '{{ Form::' . $typeHtml . '(\'' . trim($columnName) . '\', @$'.$namingConvention['singular_low_name'].'->'.trim($columnName).', [\'class\' => \'form-control form-control-solid mb-4 required\',\'placeholder\'=>\''.$label.'\']) }}' . "\n";
                }
                $formEdit .= "@error('". trim($columnName) ."')<div class='text-sm text-danger'>{{ \$message }}</div>@enderror";
                $formEdit .= str_repeat("\t", 2) . '</div></div>' . "\n";
            }
        }
        $formEdit .= str_repeat("\t", 2) . '</div>'. "\n";
        
        $editStub = File::get($this->pathsAndNamespacesService->getCrudgenViewsStubCustom($templateViewsDirectory) . DIRECTORY_SEPARATOR . 'edit.stub');
        $editStub = str_replace('DummyExtends', $separateStyleAccordingToActions['create']['extends'], $editStub);
        $editStub = str_replace('DummyTitle', $separateStyleAccordingToActions['create']['section_title'], $editStub);
        $editStub = str_replace('DummySection', $separateStyleAccordingToActions['create']['section'], $editStub);
        $editStub = str_replace('DummyFormCreate', $formEdit, $editStub);
        $editStub = str_replace('DummySingularName', $namingConvention['singular_name'], $editStub);
        $editStub = str_replace('DummySingularLowName$', '$' . $namingConvention['singular_low_name'], $editStub);
        $editStub = str_replace('DummyPluralName', $namingConvention['plural_name'], $editStub);
        $editStub = str_replace('DummyPluralLowName', $namingConvention['plural_low_name'], $editStub);
        return $editStub;
    }

    public function createFileOrError($namingConvention, $contentFile, $fileName)
    {
        if (!File::exists($this->pathsAndNamespacesService->getRealpathBaseCustomViews($namingConvention) . DIRECTORY_SEPARATOR . $fileName)) {
            File::put($this->pathsAndNamespacesService->getRealpathBaseCustomViews($namingConvention) . DIRECTORY_SEPARATOR . $fileName, $contentFile);
            $this->line("<info>Created View:</info> " . $fileName);
        } else
            $this->error('View ' . $fileName . ' already exists');
    }

    public function createScriptOrError($namingConvention, $contentFile, $fileName)
    {
        if (!File::exists($this->pathsAndNamespacesService->getRealpathBaseCustomScripts($namingConvention) . DIRECTORY_SEPARATOR . $fileName)) {
            File::put($this->pathsAndNamespacesService->getRealpathBaseCustomScripts($namingConvention) . DIRECTORY_SEPARATOR . $fileName, $contentFile);
            $this->line("<info>Created Datatable Script:</info> " . $fileName);
        } else
            $this->error('Datatable Script ' . $fileName . ' already exists');
    }

    private function getHtmlType($dataType)
    {
        $textAreaInputs = ['longText', 'mediumText', 'text', 'tinyText','json','jsonb'];
        $dropDownInputs = ['enum'];
        $numberInputs = ['decimal','double','float'];
        $dateTimeInputs = ['dateTimeTz','dateTime','timestamp','timestampsTz'];
        $dateInputs = ['date'];
        $timeInputs = ['time','timeTz'];
        if (in_array($dataType, $textAreaInputs)) {
            return 'textarea';
        } else if (in_array($dataType, $dropDownInputs)) {
            return 'select';
        } else if (in_array($dataType, $numberInputs)) {
            return 'number';
        } else if (in_array($dataType, $dateTimeInputs)) {
            return 'datetimeLocal';
        } else if (in_array($dataType, $dateInputs)) {
            return 'date';
        } else if (in_array($dataType, $timeInputs)) {
            return 'time';
        } else {
            return 'text';
        }
    }
}
