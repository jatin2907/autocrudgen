<?php

namespace Oneclick\Autocrudgen;

use Illuminate\Support\ServiceProvider;

class AutocrudgenServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //publish config file
        $this->publishes([__DIR__ . '/../config/autocrudgen.php' => config_path('autocrudgen.php')],'config');

        $this->publishes([__DIR__ . '/stubs/common-component' => resource_path('views/components')],'assets');

        $this->publishes([__DIR__ . '/../js' => public_path(config('autocrudgen.js_directory'))],'assets');

        $this->publishes([__DIR__ . '/stubs/default-theme/' => resource_path('views/'.config('autocrudgen.stub_directory'))],'assets');


        //and default-layout
        // $this->publishes([__DIR__ . '/stubs/default-layout.stub' => resource_path('views/default.blade.php')]);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->mergeConfigFrom(__DIR__ . '/../config/autocrudgen.php', 'crudgen');

        $this->commands(
            'Oneclick\Autocrudgen\Console\MakeCrud',
            'Oneclick\Autocrudgen\Console\MakeViews',
            'Oneclick\Autocrudgen\Console\RemoveCrud',
            'Oneclick\Autocrudgen\Console\MakeApiCrud',
            'Oneclick\Autocrudgen\Console\RemoveApiCrud'
        );
    }
}
