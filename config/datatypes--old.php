<?php
return
    [
        'bigIncrements' => [
            'column' => ''
        ],
        'bigInteger' => [
            'column' => ''
        ],
        'binary' => [
            'column' => ''
        ],
        'boolean'  => [
            'column' => ''
        ],
        'char' => [
            'column' => '',
            'length' => 200,
        ],
        'dateTimeTz' => [
            'column' => '',
            'precision' => 0,
        ],
        'dateTime' => [
            'column' => 'created_at'
        ],
        'date' => [
            'column' => 'created_at'
        ],
        'decimal' => [
            'column' => '',
            'precision' => 8,
            'scale' => 2,
        ],
        'double' => [
            'column' => '',
            'precision' => 8,
            'scale' => 2,
        ],
        'enum' => [
            'column' => '',
            'array' => []
        ],
        'float' => [
            'column' => '',
            'precision' => 8,
            'scale' => 2,
        ],
        'foreignId' => [
            'column' => '',
        ],
        'foreignIdFor' => [
            'class' => '',
        ],
        'foreignUlid' => [
            'column' => '',
        ],
        'foreignUuid' => [
            'column' => '',
        ],
        'geometryCollection' => [
            'column' => '',
        ],
        'geometry' => [
            'column' => '',
        ],
        'id' => [
            'column' => '',
        ],
        'increments' => [
            'column' => '',
        ],
        'integer' => [
            'column' => '',
        ],
        'ipAddress' => [
            'column' => '',
        ],
        'json' => [
            'column' => '',
        ],
        'jsonb' => [
            'column' => '',
        ],
        'lineString' => [
            'column' => '',
        ],
        'longText' => [
            'column' => '',
        ],
        'macAddress' => [
            'column' => '',
        ],
        'mediumIncrements' => [
            'column' => '',
        ],
        'mediumInteger' => [
            'column' => '',
        ],
        'mediumText' => [
            'column' => '',
        ],
        'morphs'=> [
            'column' => '',
        ],
        'multiLineString' => [
            'column' => '',
        ],
        'multiPoint'  => [
            'column' => '',
        ],
        'multiPolygon'  => [
            'column' => '',
        ],
        'nullableMorphs' => [
            'column' => ''
        ],
        'nullableTimestamps' => [
            'column' => ''
        ],
        'nullableUlidMorphs' => [
            'column' => ''
        ],
        'nullableUuidMorphs' => [
            'column' => ''
        ],
        'point' => [
            'column' => ''
        ],
        'polygon' => [
            'column' => ''
        ],
        'rememberToken',
        'set' => [
            'column' => '',
            'array' => []
        ],
        'smallIncrements' => [
            'column' => ''
        ],
        'smallInteger' => [
            'column' => ''
        ],
        'softDeletesTz' => [
            'column' => '',
            'precision' => 0
        ],
        'softDeletes' => [
            'column' => '',
            'precision' => 0
        ],
        'string' => [
            'column' => '',
            'length' => 100,
        ],
        'text' => [
            'column' => ''
        ],
        'timeTz'  => [
            'column' => '',
            'precision' => 0
        ],
        'time'  => [
            'column' => '',
            'precision' => 0
        ],
        'timestampTz'  => [
            'column' => '',
            'precision' => 0
        ],
        'timestamp'  => [
            'column' => '',
            'precision' => 0
        ],
        'timestampsTz' => [
            'precision' => 0
        ],
        'timestamps' => [
            'precision' => 0
        ],
        'tinyIncrements' => [
            'column' => '',
            'precision' => 0
        ],
        'tinyInteger'  => [
            'column' => '',
            'precision' => 0
        ],
        'tinyText' => [
            'column' => '',
            'precision' => 0
        ],
        'unsignedBigInteger' => [
            'column' => '',
        ],
        'unsignedDecimal' => [
            'column' => '',
            'precision' => 8,
            'scale' => 2,
        ],
        'unsignedInteger' => [
            'column' => '',
        ],
        'unsignedMediumInteger' => [
            'column' => '',
        ],
        'unsignedSmallInteger' => [
            'column' => '',
        ],
        'unsignedTinyInteger' => [
            'column' => '',
        ],
        'ulidMorphs' => [
            'column' => '',
        ],
        'uuidMorphs' => [
            'column' => '',
        ],
        'ulid' => [
            'column' => '',
        ],
        'uuid' => [
            'column' => '',
        ],
        'year' => [
            'column' => '',
        ],
    ];
