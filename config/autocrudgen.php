<?php

return
    [
        'views_style_directory' => 'default-theme',
        'separate_style_according_to_actions' =>
        [
            'index' =>
            [
                'extends' => 'layouts.app',
                'section' => 'content',
                'section_title' => 'title',
            ],
            'create' =>
            [
                'extends' => 'layouts.app',
                'section' => 'content',
                'section_title' => 'title',
            ],
            'edit' =>
            [
                'extends' => 'layouts.app',
                'section' => 'content',
                'section_title' => 'title',
            ],
            'show' =>
            [
                'extends' => 'layouts.app',
                'section' => 'content',
                'section_title' => 'title',
            ],
        ],
        'js_directory' => 'js',
        'view_directory' => 'layouts',
        'stub_directory' => 'layouts/crud',
        'middleware' => 'auth',
        'route_file' => 'web.php',
    ];
